# PIC10C Final Project

This project works on coding a 2D game I call "Tower Buster." Its fundamentally based on tower defense structure games, but instead of building towers to defend against incoming enemies, the player can purchase units to march and destroy set towers on the map.
The game works by allocating a base amount of "coins" to the player. The player can use these coins to purchase units that travel on the road, which stop to attack towers if they come into range of the towers.
Destroying each tower nets the player more coins, which they can spend immediately or save for the next rounds. After clearing a level, a set amount of coins will be rewarded to the player and the next level begins.
For this base prototype of the game, the levels' maps are the same, but the towers increase in strength on higher levels. 
Furthermore, if all units die, and the player can no longer buy any more units, the game ends. 
This game is based on Qt Creator, and uses many of the libraries provided by Qt in order to run.

For more information on the process of selecting this project, as well as some of the resources used to obtain graphics/knowledge of how to use Qt to create the game, refer to the "Final Project Decision Reasoning Log" text file.

# Things Needed to Implement

The code in this project cover all the requirements of the game besides recognizing a game over. Therefore, it still needs to have an implementation for either a game over loss or a level complete.
The reason why these were not completed is due to my inability to discover how to check what objects still exist within the game. For a game over scenario, I would need to check to see if any unit objects still exist.
For a level complete, I need to check if any tower objects exist.

# Ways to Improve the Game

There are various aspects of the game that could be modified to make it more seemless and appealing.
1.) Implementation avoiding stacking. Add some way to prevent units from overlapping in the future.
2.) Build new maps so each level has an even more interactive aspect for the player.
3.) Add more units to give the player  more variety when playing
4.) Add sound effects
5.) Trimming graphics so the block outlines do not stand out
6.) Add health bars to both units and towers
7.) Implement an automatic level up system for each level (randomizing and leveling up various towers inside without hard implementations of each tower)