#ifndef UNITBULLET_H
#define UNITBULLET_H

#include <QGraphicsPixmapItem>
#include <QObject>
#include <QGraphicsItem>

class unitBullet: public QObject, public QGraphicsPixmapItem{
    Q_OBJECT
public:
    unitBullet(QGraphicsItem *parent = 0);
    void set_strength(int str);
public slots:
    void move();
private:
    int strength;
};

#endif // UNITBULLET_H
