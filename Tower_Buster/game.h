#ifndef GAME_H
#define GAME_H

#include <QGraphicsView>
#include <QMouseEvent>
#include "coin.h"

class Game: public QGraphicsView{
public:
    // member functions
    Game();
    void mousePressEvent(QMouseEvent *event);
    void createRoad();

    // member attributes
    QGraphicsScene *scene;
    QList<QPointF> path;
    Coin *coin;
};

#endif // GAME_H
