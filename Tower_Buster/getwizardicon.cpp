#include "getWizardIcon.h"
#include "game.h"
#include "units.h"

extern Game *game;

getWizardIcon::getWizardIcon(QGraphicsItem *parent): QGraphicsPixmapItem (parent){
    setPixmap(QPixmap(":/images/WizardIcon.png"));
}

void getWizardIcon::mousePressEvent(QGraphicsSceneMouseEvent *event){
    Wizard *w = new Wizard(game->path);
    w->setPos(QPointF(0, 600));
    int cost = w->get_cost();
    if (game->coin->get_amount() >= cost)
    {
        game->coin->change_amount(-1 * cost);
        game->scene->addItem(w);
    }
    else {
        delete w;
    }
}
