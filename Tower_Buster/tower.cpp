#include "tower.h"
#include <QPixmap>
#include <QVector>
#include <QPointF>
#include <QPolygonF>
#include <QPointF>
#include <QLineF>
#include "game.h"
#include <QTimer>
#include <QGraphicsRectItem>
#include "bullet.h"
#include "units.h"
#include <QPen>

extern Game *game;

Tower::Tower(QGraphicsItem *parent, int lvl): QObject(), QGraphicsPixmapItem(parent){
    level = lvl;
    //used to scale pixels later
    int SCALE_FACTOR = 100;

    //used to allign attack_radius and tower later
    QPointF poly_center(1.5,1.5);
    QPointF tower_center(x() + 50,y() + 50);

    // create points vector
    QVector<QPointF> points;
    points << QPoint(1,0) << QPoint(2,0) << QPoint(3,1) << QPoint(3,2)
           << QPoint(2,3) << QPoint(1,3) << QPoint(0,2) << QPoint(0,1);

    if (level == 1){
        // set the graphics
        setPixmap(QPixmap(":/images/ArcherTowerLvl1.png"));
        health = 5000;
        power = 20;

        // scale points
        for (size_t i = 0; i < points.size(); i++){
            points[i] *= SCALE_FACTOR;
        }

        // create the QGraphicsPolygonItem
        attack_area = new QGraphicsPolygonItem(QPolygonF(points),this);

        // move the polygon
        poly_center *= SCALE_FACTOR;
        poly_center = mapToScene(poly_center);
        QLineF ln(poly_center, tower_center);
        attack_area->setPos(x() + ln.dx(), y() + ln.dy());
    }
    if (level == 2){
        // set the graphics
        setPixmap(QPixmap(":/images/ArcherTowerLvl2.png"));
        health = 6000;
        power = 25;

        // scale points
        for (size_t i = 0; i < points.size(); i++){
            points[i] *= SCALE_FACTOR;
        }

        // create the QGraphicsPolygonItem
        attack_area = new QGraphicsPolygonItem(QPolygonF(points),this);

        // move the polygon
        poly_center *= SCALE_FACTOR;
        poly_center = mapToScene(poly_center);
        QLineF ln(poly_center, tower_center);
        attack_area->setPos(x() + ln.dx(), y() + ln.dy());
    }
    if (level == 3){
        // set the graphics
        setPixmap(QPixmap(":/images/ArcherTowerLvl3.png"));
        health = 7000;
        power = 30;

        // scale points
        SCALE_FACTOR = 120;
        for (size_t i = 0; i < points.size(); i++){
            points[i] *= SCALE_FACTOR;
        }

        // create the QGraphicsPolygonItem
        attack_area = new QGraphicsPolygonItem(QPolygonF(points),this);

        // move the polygon
        poly_center *= SCALE_FACTOR;
        poly_center = mapToScene(poly_center);
        QLineF ln(poly_center, tower_center);
        attack_area->setPos(x() + ln.dx(), y() + ln.dy());
    }
    if (level == 4){
        // set the graphics
        setPixmap(QPixmap(":/images/ArcherTowerLvl4.png"));
        health = 8000;
        power = 35;

        // scale points
        SCALE_FACTOR = 120;
        for (size_t i = 0; i < points.size(); i++){
            points[i] *= SCALE_FACTOR;
        }

        // create the QGraphicsPolygonItem
        attack_area = new QGraphicsPolygonItem(QPolygonF(points),this);

        // move the polygon
        poly_center *= SCALE_FACTOR;
        poly_center = mapToScene(poly_center);
        QLineF ln(poly_center, tower_center);
        attack_area->setPos(x() + ln.dx(), y() + ln.dy());
    }
    if (level == 5){
        // set the graphics
        setPixmap(QPixmap(":/images/ArcherTowerLvl5.png"));
        health = 9000;
        power = 40;

        // scale points
        SCALE_FACTOR = 160;
        for (size_t i = 0; i < points.size(); i++){
            points[i] *= SCALE_FACTOR;
        }

        // create the QGraphicsPolygonItem
        attack_area = new QGraphicsPolygonItem(QPolygonF(points),this);

        // move the polygon
        poly_center *= SCALE_FACTOR;
        poly_center = mapToScene(poly_center);
        QLineF ln(poly_center, tower_center);
        attack_area->setPos(x() + ln.dx(), y() + ln.dy());
    }
    if (level == 6){
        // set the graphics
        setPixmap(QPixmap(":/images/ArcherTowerFinalLvl.png"));
        health = 10000;
        power = 50;

        // scale points
        SCALE_FACTOR = 160;
        for (size_t i = 0; i < points.size(); i++){
            points[i] *= SCALE_FACTOR;
        }

        // create the QGraphicsPolygonItem
        attack_area = new QGraphicsPolygonItem(QPolygonF(points),this);

        // move the polygon
        poly_center *= SCALE_FACTOR;
        poly_center = mapToScene(poly_center);
        QLineF ln(poly_center, tower_center);
        attack_area->setPos(x() + ln.dx(), y() + ln.dy());
    }
    //set attack_area to be less defined
    attack_area->setPen(QPen(Qt::DotLine));

    //connect timer to attack target
    QTimer *timer = new QTimer();
    connect(timer, SIGNAL(timeout()), this, SLOT(acquire_target()));
    timer->start(1000);
}

void Tower::level_up(){
    level++;
}

double Tower::distanceTo(QGraphicsItem *item){
    QLineF ln(pos(), item->pos());
    return ln.length();
}

void Tower::take_damage(int str){
    health -= str;
    if (health <= 0){
        if (level == 1){
            game->coin->change_amount(500);
        }
        if (level == 2){
            game->coin->change_amount(750);
        }
        if (level == 3){
            game->coin->change_amount(1000);
        }
        if (level == 4){
            game->coin->change_amount(1250);
        }
        if (level == 5){
            game->coin->change_amount(1500);
        }
        if (level == 6){
            game->coin->change_amount(2000);
        }
        game->scene->removeItem(this);
        delete this;
    }
}

int Tower::get_health() const{
    return health;
}

void Tower::fire(){
    Bullet *bullet = new Bullet();
    bullet->setPos(x() + 50, y() + 50);
    bullet->set_strength(power);

    QLineF ln(QPointF(x() + 50, y() + 50), atkdest);
    double angle = -1 * ln.angle();
    bullet->setRotation(angle);
    game->scene->addItem(bullet);
}

void Tower::acquire_target(){
    //get list of items colliding with attack area
    QList<QGraphicsItem *> colliding_items = attack_area->collidingItems();

    //if no enemies in list, do not attack
    if (colliding_items.size() == 1){
        has_target = false;
        return;
    }

    double closest_dist = 300; //current variable meant to be replaced
    QPointF closest_pt = QPointF(0, 0);
    for (size_t i = 0; i < colliding_items.size(); i++){
        // if cast is unsuccessful, unit = nullptr. Otherwise, it is a unit
        Units *unit = dynamic_cast<Units *>(colliding_items[i]);
        if (unit){
            double this_dist = distanceTo(unit);
            if (this_dist < closest_dist){
                closest_dist = this_dist;
                closest_pt = colliding_items[i]->pos();
                // make the aim go towards center of icons instead of corner
                closest_pt.setX(closest_pt.x() + 30);
                has_target = true;
                atkdest = closest_pt;
                fire();
            }
        }
    }

}
