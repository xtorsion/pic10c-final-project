#ifndef UNITICONS_H
#define UNITICONS_H

#include <QGraphicsPixmapItem>
#include <QGraphicsSceneMouseEvent>

class getInfantryIcon: public QGraphicsPixmapItem{
public:
    getInfantryIcon(QGraphicsItem *parent = 0);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
};

#endif // UNITICONS_H
