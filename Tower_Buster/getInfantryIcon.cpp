#include "getInfantryIcon.h"
#include "game.h"
#include "units.h"

extern Game *game;

getInfantryIcon::getInfantryIcon(QGraphicsItem *parent): QGraphicsPixmapItem (parent){
    setPixmap(QPixmap(":/images/InfantryIcon.png"));
}

void getInfantryIcon::mousePressEvent(QGraphicsSceneMouseEvent *event){
    Infantry *i = new Infantry(game->path);
    i->setPos(QPointF(0, 600));
    int cost = i->get_cost();
    if (game->coin->get_amount() >= cost)
    {
        game->coin->change_amount(-1 * cost);
        game->scene->addItem(i);
    }
    else {
        delete i;
    }
}
