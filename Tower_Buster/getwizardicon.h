#ifndef GETWIZARDICON_H
#define GETWIZARDICON_H

#include <QGraphicsPixmapItem>
#include <QGraphicsSceneMouseEvent>

class getWizardIcon: public QGraphicsPixmapItem{
public:
    getWizardIcon(QGraphicsItem *parent = 0);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
};

#endif // GETWIZARDICON_H
