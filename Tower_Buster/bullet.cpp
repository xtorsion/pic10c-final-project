#include "bullet.h"
#include <QPixmap>
#include <QTimer>
#include <qmath.h>
#include <QList>
#include "units.h"
#include "game.h"


Bullet::Bullet(QGraphicsItem *parent)
{
    // set graphics
    setPixmap(QPixmap(":/images/arrow.png"));

    strength = 0;
    //connect a timer to move
    QTimer *move_timer = new QTimer(this);
    connect(move_timer, SIGNAL(timeout()), this, SLOT(move()));
    move_timer->start(50);
}

void Bullet::set_strength(int str){
    strength = str;
}

void Bullet::move(){
    // if bullet collides with enemy, deal damage and destroy bullet
    QList<QGraphicsItem *> colliding_items = collidingItems();
    for (size_t i = 0; i < colliding_items.size(); i++){
        // if cast is unsuccessful, unit = nullptr. Otherwise, it is a unit
        Units *unit = dynamic_cast<Units *>(colliding_items[i]);
        if (unit){
            // remove bullet
            scene()->removeItem(this);
            unit->take_damage(strength);
            delete this;
            return;
        }
    }

    if (x() > 1400 || y() > 800){
        scene()->removeItem(this);
        delete this;
        return;
    }

    int STEP_SIZE = 30;
    // move at current angle
    double theta = rotation(); //degrees
    double dy = STEP_SIZE * qSin(qDegreesToRadians(theta));
    double dx = STEP_SIZE * qCos(qDegreesToRadians(theta));

    setPos(x() + dx, y() + dy);
}
