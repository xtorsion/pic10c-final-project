#ifndef UNITS_H
#define UNITS_H

#include <QGraphicsPixmapItem>
#include <QObject>
#include <QList>
#include <QPointF>
#include <QGraphicsPolygonItem>

class Units: public QObject, public QGraphicsPixmapItem{
    Q_OBJECT
public:
    Units();
    Units(QList<QPointF> path, QGraphicsItem *parent = 0);
    void rotateToPoint(QPointF p);
    int get_health() const;
    void take_damage(int dmg);
    int get_cost() const;
    bool in_range();
    virtual void attack() = 0;
public slots:
    void move_forward();
    void acquire_target();
protected:
    QList<QPointF> points;
    bool attacking;
    int health;
    QGraphicsPolygonItem *attack_range;
    QPointF atkdest;
    int strength;
    int cost;
private:
    QPointF dest;
    int point_index;
};

class Infantry: public Units{
    Q_OBJECT
public:
    Infantry(QList<QPointF> path, QGraphicsItem *parent = 0);
    virtual void attack();
public slots:
    void attack_animation();
};

class Archer: public Units{
public:
    Archer(QList<QPointF> path, QGraphicsItem *parent = 0);
    virtual void attack();
};

class Wizard: public Units{
public:
    Wizard(QList<QPointF> path, QGraphicsItem *parent = 0);
    virtual void attack();
};

#endif // UNITS_H
