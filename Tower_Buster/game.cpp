#include "game.h"
#include <QGraphicsScene>
#include "tower.h"
#include "bullet.h"
#include "units.h"
#include "getInfantryIcon.h"
#include "getArcherIcon.h"
#include "getWizardIcon.h"
#include <QGraphicsLineItem>
#include <QBrush>
#include <QImage>

Game::Game(): QGraphicsView(){
    // create a scene
    scene = new QGraphicsScene(this);
    scene->setSceneRect(0,0,1400,800);
    setBackgroundBrush(QBrush(QImage(":/images/background.png")));

    // set the scene
    setScene(scene);

    // create unit travel path
    path << QPointF(0, 600) << QPointF(400, 600) << QPointF(400, 400) << QPointF(700, 400)
         << QPointF(700, 200) << QPointF(1000, 200) << QPointF(1000, 400) << QPointF(1400, 400);

    // create a road
    createRoad();

    // create coin count
    coin = new Coin();
    scene->addItem(coin);

    // create a tower
    Tower *base = new Tower();
    Tower *t1 = new Tower();
    Tower *t2 = new Tower();
    Tower *t3 = new Tower(base, 2);
    Tower *t4 = new Tower();
    t1->setPos(275, 475);
    t2->setPos(575, 275);
    t3->setPos(725, 225);
    t4->setPos(875, 225);

    // add the tower to scene
    scene->addItem(t1);
    scene->addItem(t2);
    scene->addItem(t3);
    scene->addItem(t4);

    setFixedSize(1400,800);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    //create icons for summoning
    getInfantryIcon *ii = new getInfantryIcon();
    getArcherIcon *ai = new getArcherIcon();
    getWizardIcon *wi = new getWizardIcon();
    ii->setPos(x(), y() + 740);
    ai->setPos(x() + 60, y() + 740);
    wi->setPos(x() + 120, y() + 740);

    scene->addItem(ii);
    scene->addItem(ai);
    scene->addItem(wi);
}

//passes event clicks to respective icon class
void Game::mousePressEvent(QMouseEvent *event){
    QGraphicsView::mousePressEvent(event);
}

void Game::createRoad(){
// connects up to second last point (which connects to last point)
for (size_t i = 0; i < path.size() - 1; i++){
    //create a line connecting the two points
    QLineF line(path[i], path[i+1]);
    QGraphicsLineItem *road = new QGraphicsLineItem(line);

    QPen pen;
    pen.setWidth(30);
    pen.setColor(Qt::darkGray);
    road->setPen(pen);

    scene->addItem(road);
}
}
