#include "coin.h"
#include <QString>
#include <QFont>

Coin::Coin(QGraphicsItem *parent): QGraphicsTextItem(parent) {
    //initialize coin to 500
    amount = 500;
    //draw the text
    setPlainText(QString("Coins: ") + QString::number(amount));
    setDefaultTextColor(Qt::yellow);
    setFont(QFont("times", 16));
}

void Coin::change_amount(int amt){
    if (amount + amt >= 0){
        amount += amt;
        setPlainText(QString("Coins: ") + QString::number(amount));
    }
}

int Coin::get_amount() const{
    return amount;
}

bool Coin::can_subtract(int amt){
    if (amount - amt >= 0){
        return true;
    }
    else {
        return false;
    }
}
