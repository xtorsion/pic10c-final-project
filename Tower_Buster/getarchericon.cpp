#include "getArcherIcon.h"
#include "game.h"
#include "units.h"

extern Game *game;

getArcherIcon::getArcherIcon(QGraphicsItem *parent): QGraphicsPixmapItem (parent){
    setPixmap(QPixmap(":/images/ArcherIcon.png"));
}

void getArcherIcon::mousePressEvent(QGraphicsSceneMouseEvent *event){
    Archer *a = new Archer(game->path);
    a->setPos(QPointF(0, 600));
    int cost = a->get_cost();
    if (game->coin->get_amount() >= cost)
    {
        game->coin->change_amount(-1 * cost);
        game->scene->addItem(a);
    }
    else {
        delete a;
    }
}
