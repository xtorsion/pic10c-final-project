#include "units.h"
#include <QPixmap>
#include <QTimer>
#include <qmath.h>
#include "game.h"
#include "tower.h"
#include "unitBullet.h"

extern Game *game;

Units::Units(){
    points = game->path;
    point_index = 0;
    dest = points[point_index];
    rotateToPoint(dest);
    attacking = false;
    health = 100;
    strength = 0;
    cost = 0;
}

Units::Units(QList<QPointF> path, QGraphicsItem *parent){
    //set points
    points = path;
    point_index = 0;
    dest = points[point_index];
    rotateToPoint(dest);
    attacking = false;
}

void Units::rotateToPoint(QPointF p){
    QLineF ln(pos(), p);
    setRotation(-1 * ln.angle());
}

int Units::get_health() const{
    return health;
}

void Units::take_damage(int dmg){
    health -= dmg;
    if (health <= 0){
        scene()->removeItem(this);
        delete this;
    }
}

int Units::get_cost() const{
    return cost;
}

bool Units::in_range(){
    QList<QGraphicsItem *> colliding_items = attack_range->collidingItems();
    for (size_t i = 0; i < colliding_items.size(); i++){
    Tower *tower = dynamic_cast<Tower *>(colliding_items[i]);
        if (tower)
            return true;
    }
    return false;
}

void Units::move_forward(){
    //if close to dest, rotate to next dest
    //if in range of tower, attack
    if (in_range()){
        QTimer *atktimer = new QTimer();
        connect(atktimer, SIGNAL(timeout()), this, SLOT(acquire_target()));
        atktimer->start(2000);
    }
    //otherwise, travel forward
    else{
        QLineF ln(pos(), dest);
        if (ln.length() < 5){
            point_index++;
            if (point_index >= points.size()){
                return;
            }
            dest = points[point_index];
            rotateToPoint(dest);
        }

        int STEP_SIZE = 5;
        // move at current angle
        double theta = rotation(); //degrees
        double dy = STEP_SIZE * qSin(qDegreesToRadians(theta));
        double dx = STEP_SIZE * qCos(qDegreesToRadians(theta));

        setPos(x() + dx, y() + dy);
    }

}

void Units::acquire_target(){
    QList<QGraphicsItem *> colliding_items = attack_range->collidingItems();

    QPointF target = QPointF(0, 0);
    for (size_t i = 0; i < colliding_items.size(); i++){
        // if cast is unsuccessful, unit = nullptr. Otherwise, it is a unit
        Tower *tower = dynamic_cast<Tower *>(colliding_items[i]);
        if (tower){
            target = colliding_items[i]->pos();
            attacking = true;
            atkdest = target;
            attack();
            tower->take_damage(strength);
        }
    }
}

Infantry::Infantry(QList<QPointF> path, QGraphicsItem *parent){
    //set graphics
    setPixmap(QPixmap(":/images/Infantry.png"));
    points = path;

    //set unit stats
    health = 150;
    strength = 35;
    cost = 100;

    //connect timer to move_forward
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(move_forward()));
    timer->start(100);

    QPointF poly_center(1.5,1.5);
    QPointF infantry_center(x() + 25, y() + 30);

    // create points vector
    QVector<QPointF> points;
    points << QPoint(1,0) << QPoint(2,0) << QPoint(3,1) << QPoint(3,2)
           << QPoint(2,3) << QPoint(1,3) << QPoint(0,2) << QPoint(0,1);
    // scale points
    for (size_t i = 0; i < points.size(); i++){
        points[i] *= 50;
    }

    // create the QGraphicsPolygonItem
    attack_range = new QGraphicsPolygonItem(QPolygonF(points),this);

    // move the polygon
    poly_center *= 50;
    QLineF ln(poly_center, infantry_center);
    attack_range->setPos(x() + ln.dx(), y() + ln.dy());
    //set attack_area to be less defined
    attack_range->setPen(QPen(Qt::DotLine));
}

void Infantry::attack(){
    QTimer *timer = new QTimer();
    connect(timer, SIGNAL(timeout()), this, SLOT(attack_animation()));
    timer->start(2000);
}

void Infantry::attack_animation(){
    if (this->boundingRect().height() == 60){
        setPixmap(QPixmap(":/images/InfantryAtk.png"));
    }
    else {
        setPixmap(QPixmap(":/images/Infantry.png"));
    }
}

Archer::Archer(QList<QPointF> path, QGraphicsItem *parent){
    //set graphics
    setPixmap(QPixmap(":/images/Archer.png"));
    points = path;

    //set archer stats
    health = 100;
    strength = 25;
    cost = 200;

    //connect timer to move_forward
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(move_forward()));
    timer->start(150);

    QPointF poly_center(1.5,1.5);
    QPointF archer_center(x() + 30, y() + 30);

    // create points vector
    QVector<QPointF> points;
    points << QPoint(1,0) << QPoint(2,0) << QPoint(3,1) << QPoint(3,2)
           << QPoint(2,3) << QPoint(1,3) << QPoint(0,2) << QPoint(0,1);
    // scale points
    for (size_t i = 0; i < points.size(); i++){
        points[i] *= 80;
    }

    // create the QGraphicsPolygonItem
    attack_range = new QGraphicsPolygonItem(QPolygonF(points),this);

    // move the polygon
    poly_center *= 80;
    QLineF ln(poly_center, archer_center);
    attack_range->setPos(x() + ln.dx(), y() + ln.dy());
    //set attack_area to be less defined
    attack_range->setPen(QPen(Qt::DotLine));
}

void Archer::attack(){
    unitBullet *bullet = new unitBullet();
    bullet->setPos(x() + 30, y() + 30);
    bullet->set_strength(strength);

    QLineF ln(QPointF(x() + 30, y() + 30), atkdest);
    double angle = -1 * ln.angle();
    bullet->setRotation(angle);
    game->scene->addItem(bullet);
}

Wizard::Wizard(QList<QPointF> path, QGraphicsItem *parent){
    //set graphics
    setPixmap(QPixmap(":/images/Wizard.png"));
    points = path;

    // set unit stats
    health = 80;
    strength = 60;
    cost = 500;

    //connect timer to move_forward
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(move_forward()));
    timer->start(200);

    QPointF poly_center(1.5,1.5);
    QPointF wizard_center(x() + 30, y() + 30);

    // create points vector
    QVector<QPointF> points;
    points << QPoint(1,0) << QPoint(2,0) << QPoint(3,1) << QPoint(3,2)
           << QPoint(2,3) << QPoint(1,3) << QPoint(0,2) << QPoint(0,1);
    // scale points
    for (size_t i = 0; i < points.size(); i++){
        points[i] *= 100;
    }

    // create the QGraphicsPolygonItem
    attack_range = new QGraphicsPolygonItem(QPolygonF(points),this);

    // move the polygon
    poly_center *= 100;
    QLineF ln(poly_center, wizard_center);
    attack_range->setPos(x() + ln.dx(), y() + ln.dy());
    //set attack_area to be less defined
    attack_range->setPen(QPen(Qt::DotLine));
}

void Wizard::attack(){
    unitBullet *bullet = new unitBullet();
    bullet->setPixmap(QPixmap(":/images/Fireball.png"));
    bullet->setPos(x() + 30, y() + 30);
    bullet->set_strength(strength);

    QLineF ln(QPointF(x() + 30, y() + 30), atkdest);
    double angle = -1 * ln.angle();
    bullet->setRotation(angle);
    game->scene->addItem(bullet);
}
