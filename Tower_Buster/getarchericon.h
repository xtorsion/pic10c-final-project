#ifndef GETARCHERICON_H
#define GETARCHERICON_H

#include <QGraphicsPixmapItem>
#include <QGraphicsSceneMouseEvent>

class getArcherIcon: public QGraphicsPixmapItem{
public:
    getArcherIcon(QGraphicsItem *parent = 0);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
};

#endif // GETARCHERICON_H
