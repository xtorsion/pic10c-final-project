#ifndef COIN_H
#define COIN_H

#include <QGraphicsTextItem>

class Coin: public QGraphicsTextItem{
public:
    Coin(QGraphicsItem *parent = 0);
    void change_amount(int amt);
    int get_amount() const;
    bool can_subtract(int amt);
private:
    int amount;
};

#endif // COIN_H
