#ifndef TOWER_H
#define TOWER_H


#include <QGraphicsPixmapItem>
#include <QGraphicsPolygonItem>
#include <QGraphicsItem>
#include <QObject>
#include <QPointF>

class Tower: public QObject, public QGraphicsPixmapItem{
    Q_OBJECT
public:
    Tower(QGraphicsItem *parent=0, int lvl = 1);
    void level_up();
    void fire();
    double distanceTo(QGraphicsItem *item);
    void take_damage(int str);
    int get_health() const;
public slots:
    void acquire_target();
private:
    QGraphicsPolygonItem *attack_area;
    int level;
    QPointF atkdest;
    bool has_target;
    int health;
    int power;
};


#endif // TOWER_H
