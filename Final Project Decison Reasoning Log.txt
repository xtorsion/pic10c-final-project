Initial Thoughts/Ideas:

Premise:
Ever since I was young, gaming has been a big part of my life. I have always enjoyed playing all sorts of various games, and would like to translate this passion into my final project design.

Game ideas:
Simple FPS, creating more of a target practice world, with a basic 3D map design, and creating target dummies that respawn after a set amount of time.
	Key things are to implement a 3D world, thus, moving the mouse will lead to a rotation of the map. No up or down viewing will be applied, so just walking left right forward backwards, and aiming by rotating the character.
A phone application type game, creating something more relevant that could be expanded on, developing a game that can be used concurrently with mobile devices. 
	Specific ideas: Tower buster (opposite of tower defense, summon units that deal certain damage outputs to attack tower formations that are designed prior, per level.

Expansion of Ideas:

Note: I wanted to focus my project on using QTCreator, as that was one of the driving engines to building a game during the class.

Simple FPS:
Discovered tutorial on creation of a First Person world:
https://www.youtube.com/watch?v=xW8skO7MFYw
Ray casting decided as best method to design 3D elements
https://en.wikipedia.org/wiki/Ray_casting
Issues with using QTCreator for 3D games (current tutorial makes use of the console output instead)
https://forum.qt.io/topic/19764/first-person-shooter-using-opengl-and-qt/2

Pursued for a bit, but the details about 3D elements and their use in QT became too much for me, so opted to choose second idea instead.

Phone Application game:

Tower Buster Idea:
How the game works:
At the start of the level, a certain amount of money will be provided to the player. They can use this money to purchase select "units" that each have different properties. 
After units are selected, the player can hit "play," which summons the selected units onto the map, which has turrets/towers along the path. The units attack said towers, and for each destroyed building, more money is earned, which they can spend to purchase units at any time.
Goal is to defeat all the towers in each level, and game ends if all summoned units die, and player cannot summon any more.

Since this project will be done in QT, the bulk of the assignment is understanding how to use QT Widgets and its various properties to create a more interactive and dynamic game. 
Resources:
https://doc.qt.io/qtcreator/quick-projects.html
https://doc.qt.io/qt-5/qtquick-index.html
https://doc.qt.io/qt-5/qtdoc-demos-maroon-example.html
Tutorial followed to learn more about QT Game Creation (youtube link specifically targets graphic insertion to game)
https://www.youtube.com/watch?v=xPs40BrYHkg&list=PLyb40eoxkelOa5xCB9fvGrkoBf8JzEwtV&index=8

Further Notes:
This project relates to the material learned in PIC10C for a couple of main topics. The first is git interaction, as keeping track of working commits was critical, as one error (and this occurred multiple times) caused the entire code to fail to work.
I could not even pinpoint the reasoning, but fortunately by reverting to a previous commit, no significant progress was lost.
The second huge topic was Qt Creator. This entire project is based on Qt Creator and how to use its many libraries, widgets, signals, slots, and other specific criteria. Without qt, this whole project would have been impossible to create.
Other topics that were touched were inheritence and dynamic casting, which can be found to accomplish specific tasks in the code (various units, finding targets, etc.)
Topics like generic algorithms, binding, and lambda functions were very difficult to incorporate mostly because of how specific and unique qt is. For the bulk of the project, most of the coding used qt-specific commands, making generic functions and the other topics hard to incorporate.

Citations:
The practice/tutorial results are shown in the QT-Practice Branch that was pushed to the repository.
The tower sprites were taken from this pinterest: https://www.pinterest.com/pin/470696598528539754/
The arrow sprite is taken from this (and drastically resized): https://s3.envato.com/files/171719892/weaponpackprev/2000x2000/arrow1b.png
The infantry sprite is taken from this: https://www.spriters-resource.com/pc_computer/heroes3/sheet/42157/
The archer sprite is taken from this: https://opengameart.org/content/archer-static-64x64
The wizard sprite is taken from: https://craftpix.net/freebies/wizard-character-free-sprite/
The fireball sprite is from: https://www.nicepng.com/maxp/u2q8y3r5e6o0w7u2/
The background image is from: https://www.picswe.com/pics/rpg-grass-background-c7.html




